/*
	While Loop

		Syntax:

			while(expression/condition){
					statement
			}
*/
let count = 5
while (count !== 0){
	console.log("While: " + count)
	count-- 
}

// or we can use count-1= or count-=x

/*
	Mini Activity:
		The while loop should only display the numbers 1-5
		Correct the following loop

		let x = 0
		while (x < 1){
			console.log(x)
			x--;
		}



*/
let x = 1;
		while (x < 6){
			console.log(x)
			x++;
		}

/*
	DO WHILE LOOP
		A do-while loop works a lot like the while loop. But unlike while loops, do-while loops guarantee that the code will be executed at least once.

		Syntax:
			do{
				statement
			} while (expression/condition)

*/

let number = Number(prompt("Give me a number: "))
do {
	console.log("Do While " + number)
	number += 1
} while(number < 10)

/*
	FOR LOOP
	 - more flexible than while loop  and do while loop
	 Syntax:
	 	for (initialization, expression/condition; finalExpression){
			statement
	 	}
*/
for(let count = 19; count <= 20; count++){
	console.log("For Loop: " + count)
}

let myString = "Enteng Kabisote"
console.log(myString.length)
// .length counts spaces

console.log(myString[0])
console.log(myString[1])
console.log(myString[14])

for(let x = 0; x < myString.length; x++){
	console.log(myString[x])
}

let myName = "Jeremiah";

for(let i = 0; i < myName.length; i++){
	if(
		myName[i].toLowerCase() == "a" ||
		myName[i].toLowerCase() == "e" ||
		myName[i].toLowerCase() == "i" ||
		myName[i].toLowerCase() == "o" ||
		myName[i].toLowerCase() == "u" 
		// || represents "or"
		){
		console.log("Vowel")
	}else{console.log(myName[i])}
}

// continue and break statements
/*
	Continue statement allows the code to go to the next iteration of the loop without finishing the execution of all statements in a code block

	Break statement is used to terminate the current loop once a match has been found.

*/

for (let count = 0; count <= 20; count++){
	if (count % 2 === 0){
		console.log("Even Number")
		continue;
	}
	console.log("Continue and Break: " + count)

	if (count > 10) {
		break;
	}
}

let name = "Alexandro"
for (let i = 0; i < name.length; i++){
	console.log(name[i]);
	if(name[i].toLowerCase() === "a"){
		console.log("Continue to the next iteration")
		continue;
	}
	console.log("Hello")
	if(name[i].toLowerCase() === "d"){
		break;
	}
}